package nl.bioinf.hadonkerbroek.assignment1.model;

/**
 zone 1: 50-60% Low heart rate with hardly any training effect
 zone 2: 60-70% Heart rate with positive effect on breathing and stress reduction
 zone 3: 70-80% Heart rate with optimal training effect with respect to fat burning, general condition and brain functioning
 zone 4: 80-90% Zone for improving (long-distance) running speed
 zone 5: 90-100% Above anaerobic threshold; only during interval training to increase speed (also for shorter distances).
 */

public class ZoneCalculator {
    private String zoneOne;
    private String zoneTwo;
    private String zoneThree;
    private String zoneFour;
    private String zoneFive;

    public ZoneCalculator() {}

    public ZoneCalculator(double heart_rate) {
        this.zoneOne = String.format(
                "%s = %d - %d",
                "zone 1: 50-60% Low heart rate with hardly any training effect.",
                Math.round(heart_rate * 0.5),
                Math.round(heart_rate * 0.6));
        this.zoneTwo = String.format(
                "%s = %d - %d",
                "zone 2: 60-70% Heart rate with positive effect on breathing and stress reduction.",
                Math.round(heart_rate * 0.6),
                Math.round(heart_rate * 0.7));
        this.zoneThree = String.format(
                "%s = %d - %d",
                "zone 3: 70-80% Heart rate with optimal training effect with respect to fat burning, general condition and brain functioning.",
                Math.round(heart_rate * 0.7),
                Math.round(heart_rate * 0.8));
        this.zoneFour = String.format(
                "%s = %d - %d",
                "zone 4: 80-90% Zone for improving (long-distance) running speed.",
                Math.round(heart_rate * 0.8),
                Math.round(heart_rate * 0.9));
        this.zoneFive = String.format(
                "%s = %d - %d",
                "zone 5: 90-100% Above anaerobic threshold; only during interval training to increase speed (also for shorter distances).",
                Math.round(heart_rate * 0.9),
                Math.round(heart_rate * 1));
    }

    public String getZoneOne() {
        return zoneOne;
    }

    public String getZoneTwo() {
        return zoneTwo;
    }

    public String getZoneThree() {
        return zoneThree;
    }

    public String getZoneFour() {
        return zoneFour;
    }

    public String getZoneFive() {
        return zoneFive;
    }
}
