package nl.bioinf.hadonkerbroek.assignment1.servlets;

import nl.bioinf.hadonkerbroek.assignment1.config.WebConfig;
import nl.bioinf.hadonkerbroek.assignment1.model.ZoneCalculator;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@WebServlet(name = "PhraseServlet", urlPatterns = "/heart-rate-zones")
public class HeartRateServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        double heartrate = Double.parseDouble(request.getParameter("heartrate"));
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);

        ZoneCalculator zoneCalculator = new ZoneCalculator(heartrate);
        ctx.setVariable("zones", zoneCalculator);

        templateEngine.process("heart-rate", ctx, response.getWriter());
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Locale locale = request.getLocale();
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                locale);
        templateEngine.process("heart-rate", ctx, response.getWriter());
    }
}